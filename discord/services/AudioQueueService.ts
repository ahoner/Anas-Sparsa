import { Client, VoiceChannel, VoiceState } from 'discord.js';
import AudioQueue from '@commonTypes/AudioQueue';
import { jingles, jingleProbability } from '@root/config.json';
import { Readable } from 'stream';

export default class AudioQueueService {
  client: Client;
  audioQueues: Map<string, AudioQueue>;
  voiceChannels: Map<string, Set<string>>;

  constructor(client: Client) {
    this.audioQueues = new Map();
    this.client = client;
    this.voiceChannels = new Map();
  }

  registerQueues(guildId: Array<string>) {
    guildId.forEach(guildId => {
      this.audioQueues.set(guildId, new AudioQueue());
      this.client.on('voiceStateUpdate', (oldState: VoiceState, newState: VoiceState) => {
        const queue = this.audioQueues.get(newState.guild.id);
        const member = newState.guild.members.cache.get(newState.id);
        const userJingles: string[] = jingles[member.id];
        let jingle: string = '';

        // Keep track of users in channels, sometimes this event is fired twice
        if (!this.voiceChannels.get(oldState.channelID))
          this.voiceChannels.set(oldState.channelID, new Set())

        if (!this.voiceChannels.get(newState.channelID))
          this.voiceChannels.set(newState.channelID, new Set())

        if (oldState.channelID !== newState.channelID)
          this.voiceChannels.get(oldState.channelID).delete(oldState.id)

        if (!newState.channelID 
          || this.voiceChannels.get(newState.channelID).has(newState.id))
          return;

        this.voiceChannels.get(newState.channelID).add(newState.id)

        if (userJingles && userJingles.length === 2) {
          const roll = Math.floor(Math.random() * 3);
          jingle = roll === 0 ? userJingles[1] : userJingles[0]
        } else if (userJingles) {
          jingle = userJingles[0]
        }
        const roll = Math.floor(Math.random() * jingleProbability);
        if (jingle
          && roll === 0)
          queue.pushInputToQueue(jingle, member.voice.channel);
      })
    });
  }

  playAudioFile(source: string | Readable, voiceChannel: VoiceChannel, guildId: string): void {
    if(!voiceChannel) 
      return;

    const queue = this.audioQueues.get(guildId);
    queue.pushInputToQueue(source, voiceChannel);
  }
}