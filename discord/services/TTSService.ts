import { Voice } from "@commonTypes/Voice";
import { Readable } from "stream";
import  { request } from 'http';

const querystring = require('querystring');

export default class TTSService {
  constructor() {}

  public  async aiRequest(voice: Voice, text: string[]) {
    const resps = await Promise.all(text.map(async (chunk) => {
      const data = {
	      text : chunk,
	      character : voice,
	      emotion : 'Contextual',
	      use_diagonal : true
      };
      return await this.doRequest('api.15.ai', '/app/getAudioFile', data)
    }))
    console.log('grabbed');
    return resps;
  }

  private async doRequest(url: string, path: string, data: any): Promise<Readable> {
    return new Promise((resolve, reject) => {
      const req = request({
        host: url,
        path: path,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        }
      }, (res) => {
        if (res.statusCode < 200 || res.statusCode >= 300)
          return reject(new Error('statusCode = ' + res.statusCode));

        const body = [];
        res.on('data', (chunk) => {
          body.push(chunk);
        });
        res.on('end', () => {
          try {
            const buffer = Buffer.concat(body);
            const readable = new Readable();
            readable._read = () => {};
            readable.push(buffer);
            readable.push(null);
            resolve(readable);
          } catch(e) {
            reject(new Error(e));
          }
        })
      });
      req.on('error', (err) => {
        reject(err);
      })
      req.write(JSON.stringify(data));
      req.end();
    })
  }
}