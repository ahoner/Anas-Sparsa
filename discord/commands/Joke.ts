import OpenAiService from "@commonServices/OpenAiService";
import { Message } from "discord.js";
import Utils from '@common/utils';
import CommandEntry from "@commonTypes/CommandEntry";

const config = require('@root/config.json');

export default class JokeCommand {
  openAiService: OpenAiService;

  constructor(
    openAiService: OpenAiService
  ) {
    this.openAiService = openAiService;
  }

  async execute(message: Message) {
    const commandEntry: CommandEntry = Utils.getCommandEntry('joke');
    const subject = Utils.selectRandom(commandEntry.subjects);
    const place = Utils.selectRandom(commandEntry.places);
    this.openAiService.joke(subject, place)
      .then(reply => {
        message.channel.send(`So ${subject} walks into ${place}${reply}`);
      })
  }
}