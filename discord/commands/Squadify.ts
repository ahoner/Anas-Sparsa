import { Message } from 'discord.js';
import Util from '@common/utils';

export default class SquadifyCommand {
  async execute(message: Message, numSquads: number): Promise<void> {
    if (!message.member.voice.channel) {
      await message.channel.send('User not in voice channel');
      return;
    }
    if (!numSquads) {
      await message.channel.send('You must specify the number of squads');
      return;
    }
    let membersInChannel = message.member.voice.channel.members.array();
    let parties = [];
    for (let i = 0; i < numSquads; i++)
      parties[i] = [];

    while (membersInChannel.length > 0) {
        const index = Math.floor(Math.random() * membersInChannel.length)
        const party = Util.minLength(parties);
        parties[party].push(membersInChannel[index].user.username);
        membersInChannel.splice(index, 1);
    }
    let text = 'The squads are :\n';
    parties.forEach((party, i) => {
      text += `Squad ${i + 1}: ${party}\n`;
    });
    message.channel.send(text);
  } 
}