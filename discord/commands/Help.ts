import { Message } from 'discord.js';
import CommandEntry from '@commonTypes/CommandEntry';

const config = require('@root/config.json');

export default class HelpCommand {
  execute(message: Message): void {
    const helpEntries: Array<CommandEntry> = config.commands.map(command => command as CommandEntry);

    const text =
      `Hello! I am a duck-based Discord bot. I am currently not very helpful.
Here is what I can do:
\`\`\`
${helpEntries.map(commandEntry => this.entryAsString(commandEntry)).join('\n')}
\`\`\``;

    message.channel.send(text);
  }

  entryAsString(commandEntry: CommandEntry): string {
    return `${config.prefix}${commandEntry.name} ${commandEntry.usage}\n\t-${commandEntry.description}`
  }
}