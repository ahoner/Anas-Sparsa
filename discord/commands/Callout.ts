import { Message } from "discord.js";

import AudioQueueService from "@discordServices/AudioQueueService";
import CommandEntry from '@commonTypes/CommandEntry';
import AudioFileCommand from "./AudioFileCommand";
import Utils from '@common/utils';

export default class CalloutCommand extends AudioFileCommand {
  constructor(audioQueueService: AudioQueueService) {
    super(audioQueueService);
  }

  async execute(message: Message) {
    const commandEntry: CommandEntry = Utils.getCommandEntry('callout');
    super.execute(message, commandEntry);
  }
}