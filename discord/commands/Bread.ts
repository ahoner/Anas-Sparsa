import { Message } from 'discord.js';

import BreadRepository from '@repos/BreadRepository';
import AudioQueueService from '@discordServices/AudioQueueService';
import CommandEntry from '@commonTypes/CommandEntry';
import Utils from '@common/utils';

export default class BreadCommand {
  breadRepo: BreadRepository;
  audioQueueService: AudioQueueService;

  constructor(
    breadRepo: BreadRepository,
    audioQueueService: AudioQueueService,
   ) {
    this.breadRepo = breadRepo;
    this.audioQueueService = audioQueueService;
  }

  async execute(message: Message): Promise<void> {
    await this.breadRepo.incrementBread();
    const count = await this.breadRepo.getBread();
    const commandEntry: CommandEntry = Utils.getCommandEntry('bread');
    const reaction = Utils.selectRandom(commandEntry.reactions);
    const soundBite = Utils.selectRandom(commandEntry.soundBites);
    this.audioQueueService.playAudioFile(soundBite, message.member.voice.channel, message.guild.id);
    message.channel.send(`Mmmm I've been fed ${count.count} pieces\n${reaction}`);
  }
}