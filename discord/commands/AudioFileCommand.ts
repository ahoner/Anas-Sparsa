import { Message } from "discord.js";

import AudioQueueService from '@discordServices/AudioQueueService';
import CommandEntry from '@commonTypes/CommandEntry';

export default class AudioFileCommand {
  audioQueueService: AudioQueueService;

  constructor(audioQueueService: AudioQueueService) {
    this.audioQueueService = audioQueueService;
  }

  async execute(message: Message, commandEntry: CommandEntry): Promise<void> {
    const voiceChannel = message.member.voice.channel;
    if (!voiceChannel)
      return;

    this.audioQueueService.playAudioFile(commandEntry.path, voiceChannel, message.guild.id);
  }
}