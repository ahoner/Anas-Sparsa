import { Message } from "discord.js";

import AudioQueueService from "@discordServices/AudioQueueService";
import Utils from '@common/utils';

export default class QuackCommand {
  audioQueueService: AudioQueueService;

  constructor(audioQueueService: AudioQueueService) {
    this.audioQueueService = audioQueueService;
  }

  async execute(message: Message): Promise<void> {
    const voiceChannel = message.member.voice.channel;
    if (!voiceChannel) {
      await message.channel.send('Honk!');
      return;
    }

    const { quacks } = Utils.getCommandEntry('quack');
    const quack = Utils.selectRandom(quacks);
    this.audioQueueService.playAudioFile(quack, voiceChannel, message.guild.id);
  }
}