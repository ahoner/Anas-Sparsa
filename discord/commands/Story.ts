import { Message, Util } from "discord.js";

import MarkovService from "@commonServices/MarkovService";
import AudioQueueService from "@discordServices/AudioQueueService";
import TTSService from "@discordServices/TTSService";
import Utils from '@common/utils';
import CommandEntry from '@commonTypes/CommandEntry';
import { Voice } from "@commonTypes/Voice";

export default class StoryCommand {
  audioQueueService: AudioQueueService;
  markovService: MarkovService;
  ttsService: TTSService;

  constructor(
    audioQueueService: AudioQueueService,
    markovService: MarkovService,
    ttsService: TTSService,
  ) {
    this.audioQueueService = audioQueueService;
    this.markovService = markovService;
    this.ttsService = ttsService;
  }

  async execute(message: Message, voice: Voice) {
    const logMessage = await message.channel.send('Generating message...');
    try {
      const commandEntry: CommandEntry = Utils.getCommandEntry('story');
      const story = await this.markovService.generateSentance(message.channel.id, commandEntry.depth);
      console.log('story generated');
      const replacedStory = await Utils.replaceIdsWithNames(message.guild, Utils.removeLinks(story));
      const chunkedStory = Utils.removeInvalidTTSChars(replacedStory).match(/.{1,198}/g);

      const voiceChannel = message.member.voice.channel;
      if (!voiceChannel)
        return;

      console.log(chunkedStory);
      this.ttsService.aiRequest(voice, chunkedStory).then(streams => {
        logMessage.edit(story);
        streams.forEach(stream => {
          this.audioQueueService.playAudioFile(stream, voiceChannel, message.guild.id);
        });
      }).catch((err) => {
        console.log(err)
        logMessage.edit(`Looks like the https://15.ai isn't working right now here's the story anyway
        ${story}`);
      });
    } catch(e) {
      console.error(e);
      logMessage.edit("I'm not feeling very creative right now :(")
    }
  }
}