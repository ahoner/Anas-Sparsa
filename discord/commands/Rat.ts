import { Message } from "discord.js";

import AudioQueueService from '@discordServices/AudioQueueService';
import CommandEntry from '@commonTypes/CommandEntry';
import AudioFileCommand from './AudioFileCommand';
import Utils from '@common/utils';

export default class RatCommand extends AudioFileCommand {
  constructor(audioQueueService: AudioQueueService) {
    super(audioQueueService);
  }

  async execute(message: Message) {
    const commandEntry: CommandEntry = Utils.getCommandEntry('rat');
    super.execute(message, commandEntry);
  }

}