import { Message } from "discord.js";
import AudioQueueService from "@discordServices/AudioQueueService";
import AudioFileCommand from "./AudioFileCommand";
import CommandEntry from '@commonTypes/CommandEntry';
import Utils from '@common/utils';

export default class HornyCommand extends AudioFileCommand {
  constructor(audioQueueService: AudioQueueService) {
    super(audioQueueService);
  }

  async execute(message: Message) {
    const commandEntry: CommandEntry = Utils.getCommandEntry('horny');
    super.execute(message, commandEntry);
  }
}