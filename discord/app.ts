import 'module-alias/register';
import {
  Client,
  Message
} from 'discord.js';

import MarkovRepository from '@repos/MarkovRepository';
import BreadRepository from '@repos/BreadRepository';

import AudioQueueService from '@discordServices/AudioQueueService';
import OpenAiService from '@commonServices/OpenAiService';
import MarkovService from '@commonServices/MarkovService';
import TTSService from '@discordServices/TTSService';

import SquadifyCommand from '@commands/Squadify';
import HellYeahCommand from '@commands/HellYeah';
import CalloutCommand from '@commands/Callout';
import ViperCommand from '@commands/Viper';
import BreadCommand from '@commands/Bread';
import HornyCommand from '@commands/Horny';
import QuackCommand from '@commands/Quack';
import StoryCommand from '@commands/Story';
import HelpCommand from '@commands/Help';
import RatCommand from '@commands/Rat';

import Utils from '@common/utils';
import { Voice } from '@commonTypes/Voice';
import JokeCommand from '@commands/Joke';

const client = new Client();
const config = require('@root/config.json');
const credentials = require('@root/credentials.json');

const breadRepo = new BreadRepository();
const markovRepo = new MarkovRepository();

const audioQueueService =  new AudioQueueService(client);
const markovService = new MarkovService(markovRepo);
const openAiService = new OpenAiService();
const ttsService = new TTSService();

const breadCommand = new BreadCommand(breadRepo, audioQueueService);
const calloutCommand = new CalloutCommand(audioQueueService);
const hellYeahCommand = new HellYeahCommand(audioQueueService);
const helpCommand = new HelpCommand();
const hornyCommand = new HornyCommand(audioQueueService);
const jokeCommand = new JokeCommand(openAiService);
const quackCommand = new QuackCommand(audioQueueService);
const ratCommand = new RatCommand(audioQueueService);
const squadifyCommand = new SquadifyCommand();
const storyCommand = new StoryCommand(audioQueueService, markovService, ttsService);
const viperCommand = new ViperCommand(audioQueueService);

client.on('ready', async () => {
  console.log('Quack Honk');
  const guildIds = Array.from(client.guilds.cache.values()).map(guild => guild.id);
  audioQueueService.registerQueues(guildIds);
})

client.on('message', async (message: Message) => {
  openAiService.addMessage(message);
  if (message.author.bot || !message.member)
    return;

  markovService.addToModel(message.channel.id, message.content.split(' '));

  if (Math.floor(Math.random() * config.markov.probability) === 0) {
    openAiService.converse(message)
      .then(reply => {
        message.channel.send(reply);
      });
  }

  if (message.content.trim().toLocaleLowerCase() === 'nice') {
    await message.channel.send(`>>> ${message.content}`);
    await message.channel.send(`<@${message.author.id}> nice`);
    return;
  }

  if (!message.content.trim().startsWith(config.prefix))
    return;

  const [command, ...args] = Utils.parseCommand(message.content);
  switch (command.toLowerCase()) {
    case undefined:
    case '':
      message.channel.send('!!!');
      break;
    
    case 'help':
      helpCommand.execute(message);
      break;

    case 'squadify':
      squadifyCommand.execute(message, Number.parseInt(args[0]));
      break;

    case 'quack':
    case 'quacc':
      quackCommand.execute(message);
      break;

    case 'horny':
      hornyCommand.execute(message);
      break;

    case 'hellyeah':
      hellYeahCommand.execute(message);
      break;

    case 'viper':
      viperCommand.execute(message);
      break;

    case 'callout':
      calloutCommand.execute(message);
      break;

    case 'story':
      const voiceParam = args[0];
      let voice: Voice;
      console.log(Object.keys(Voice))
      if (!voiceParam)
        voice = Voice[Object.keys(Voice)[Math.floor(Math.random() * Object.keys(Voice).length)]];
      else
        voice = Voice[voiceParam.toLocaleUpperCase()]

      storyCommand.execute(message, voice);
      break;

    case 'rat':
      ratCommand.execute(message);
      break;

    case 'bread':
      breadCommand.execute(message);
      break;

    case 'joke':
      jokeCommand.execute(message);
      break;
  }
  
});

client.login(credentials.token);