import BaseRepository from './BaseRepository';
import Markov from '@commonTypes/Markov';
import { query } from 'faunadb';
import { SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG } from 'constants';

const {
  Match,
  Index,
  Paginate,
  Update,
  Create,
  Collection,
  Count,
} = query;

export default class MarkovRepository extends BaseRepository {
  constructor() {
    super();
  }

  async getValuesAndRefs(channelId: string, key: string): Promise<Markov> {
    try {
      const { data } = await this.client.query(
        Paginate(
          Match(
            Index('values_by_key_and_channelId'),
            [channelId, key]
          )
        )
      );
      const values: Array<string> = Array.from(data).map(val => val[1]);
      if (!data.length)
        return {
          ref: null,
          channelId: channelId,
          values: []
        }

      return {
        ref: data[0][0],
        channelId: channelId,
        values: values
      };
    } catch (err) {
      console.error(err);
    }
  }

  async insertValue(channelId: string, key: string, value: string): Promise<void> {
    try {
      const data = await this.getValuesAndRefs(channelId, key);
      if (!data.values.length) {
        return await this.client.query(
          Create(
            Collection('markovChannelMap'), {
            data: {
              channelId: channelId,
              key: key,
              values: [value],
            }
          }
          )
        );
      }
      data.values.push(value);
      await this.client.query(
        Update(
          data.ref,
          {
            data: {
              values: data.values
            }
          }
        )
      );
    } catch (err) {
      console.error(err);
    }
  }

  async keyCount(channelId: string): Promise<number> {
    try {
      return await this.client.query(
        Count(
          Match(
            Index('keys_by_channelId'),
            channelId
          )
        )
      );
    } catch (err) {
      console.error(err);
    }
  }

  async keyAtIndex(channelId: string, index: number): Promise<string> {
    try {
      let indexFound = false;
      let count = 0;
      let { after, data } = await this.client.query(
        Paginate(
          Match(
            Index('keys_by_channelId'),
            channelId
          ), {
            size: 1000,
          }
        )
      );
      while (!indexFound) {
        if (count + data.length > index) {
          indexFound = true;
        } else {
          const result = await this.client.query(
            Paginate(
              Match(
                Index('keys_by_channelId'),
                channelId
              ), {
                size: 1000,
                after: after[0],
              }
            )
          );
          after = result.after;
          data = result.data;
          count += data.length;
        }
      }
      return data[index - count];
    } catch (err) {
      console.error(err);
    }
  }
}