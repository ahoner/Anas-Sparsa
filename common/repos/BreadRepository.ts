import BaseRepository from './BaseRepository';
import { query } from 'faunadb';
import Bread from '@commonTypes/Bread';

const {
  Paginate,
  Match,
  Index,
  Update,
} = query;

export default class BreadRepository extends BaseRepository {
  constructor() {
    super();
  }

  async getBread(): Promise<Bread> {
    try {
      const result = await this.client.query(
        Paginate(
          Match(
            Index('get_bread')
          )
        )
      );
      return {
        ref: result.data[0][0],
        count: result.data[0][1]
      };
    } catch (err) {
      console.error(err);
    }
  }

  async incrementBread(): Promise<void> {
    try {
      const bread = await this.getBread();
      await this.client.query(
        Update(
          bread.ref,
          {
            data: {
              "breads": bread.count + 1
            }
          }
        )
      )
    } catch (err) {
      console.error(err);
    }
  }
}