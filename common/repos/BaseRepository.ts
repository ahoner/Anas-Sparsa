import { devDB } from '@root/credentials.json';
const fauna = require('faunadb');

export default class BaseRepository {
  client: any;
  constructor() {
    this.client = new fauna.Client({ secret: devDB });
  }
}