import { Guild } from "discord.js";
import CommandEntry from '@commonTypes/CommandEntry';

const { commands, prefix } = require('@root/config.json');

export default class Util {
  static minLength (arr: Array<any>): number {
    let min = Number.MAX_SAFE_INTEGER;
    let minIndex = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].length < min) {
        min = arr[i].length;
        minIndex = i;
      }
    }
    return minIndex;
  }

  static parseCommand(messageContent: string): Array<string> {
    return messageContent.trim().slice(prefix.length).split(/ +/g);
  }

  static async replaceIdsWithNames(guild: Guild, str: string): Promise<string> {
    const userIdsRegex = str.match(/<@!(\d+)>/g);
    if (!userIdsRegex || userIdsRegex.length === 0)
        return str;

    const userIds = userIdsRegex.map(userIdTag => userIdTag.substring(3, userIdTag.length - 1));
    const usersPromises = userIds.map(async (id) => guild.members.cache.get(id));
    const users = await Promise.all(usersPromises);
    let editedStr = str;
    Array.from(userIds).forEach(userId => {
        const userIdTag = `<@!${userId}>`;
        const guildMember = users.find(({ user })  => user.id === userId);
        const name = guildMember.nickname ? guildMember.nickname : guildMember.user.username;
        editedStr = editedStr.replace(userIdTag, name);
    });
    return editedStr;

  }

  static removeLinks(input: string): string {
    return input.replace(/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/g, '');
  }

  static removeInvalidTTSChars(input: string): string {
    return input.replace(/[\/@#$%^&*()\+\-<>~\d]/g, '');
  }

  static getCommandEntry(commandName: string): CommandEntry {
    return commands.filter((commandEntry: CommandEntry) => commandEntry.name === commandName)[0];
  }

  static selectRandom(arr: Array<any>): any {
    return arr[Math.floor(Math.random() * arr.length)];
  }
}