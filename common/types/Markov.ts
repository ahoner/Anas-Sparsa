import FaunaResponse from './FaunaResponse';

export default interface Markov extends FaunaResponse {
  channelId?: string,
  key?: string,
  values?: Array<string>
}