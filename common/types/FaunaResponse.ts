import { query } from 'faunadb';

const {
  Ref
} = query;

export default interface FaunaResponse {
  ref: typeof Ref;
}