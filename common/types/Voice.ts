export enum Voice {
  GLADOS = 'GLaDOS',
  SCOUT = 'Scout',
  SOLDIER = 'Soldier',
  DEMOMAN = 'Demoman',
  HEAVY = 'Heavy',
  ENGINEER = 'Engineer',
  SNIPER = 'Sniper',
  SPY = 'Spy',
  TURRET = 'Sentry Turret',
  SPONGEBOB = 'SpongeBob SquarePants'
}