import TaskQueue from './TaskQueue';
import { StreamDispatcher, VoiceChannel, VoiceConnection } from 'discord.js'
import { Readable } from 'stream';

export default class AudioQueue extends TaskQueue {
  constructor() {
    super();
  }

  async pushInputToQueue(input: string | Readable, voiceChannel: VoiceChannel) {
    this.push(async () => {
      let voiceConnection: VoiceConnection;
      try {
        voiceConnection = await voiceChannel.join();
        const dispatcher: StreamDispatcher = voiceConnection.play(input);
        return new Promise((resolve) => {
          dispatcher.on('finish', () => {
            if (this.length === 0)
              voiceConnection.disconnect();

            resolve(undefined);
          });
        })
      } catch (err) {
        if (voiceConnection)
          await voiceConnection.disconnect();

        throw err;
      }
    });
  }
}