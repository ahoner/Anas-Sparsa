import TaskQueue from './TaskQueue';

const tq = new TaskQueue();

main();

async function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  })
}

async function main() {
  console.log('BEGIN TEST');

  tq.push(async () => {
    console.log('start sleep 1');
    await sleep(2000);
    console.log('end sleep 1');
  });

  await sleep(100);

  tq.push(async () => {
    console.log('start sleep 2');
    await sleep(1000);
    console.log('end sleep 2');
  });

  await sleep(100);

  tq.push(async () => {
    console.log('start sleep 3');
    await sleep(3000);
    console.log('end sleep 3');
  });
}