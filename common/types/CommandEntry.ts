export default interface ICommandEntry {
  name: string,
  usage: string,
  description: string,
  quacks?: Array<string>,
  path?: string,
  depth?: number,
  reactions?: Array<string>,
  soundBites?: Array<string>,
  subjects?: Array<string>,
  places?: Array<string>,
}