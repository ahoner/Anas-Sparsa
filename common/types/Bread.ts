import FaunaResponse from './FaunaResponse';

export default interface Bread extends FaunaResponse {
  count?: number
}