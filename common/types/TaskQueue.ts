export default class TaskQueue {
  queue: Array<() => Promise<any>>;
  running: boolean;
  
  constructor() {
    this.queue = new Array<() => Promise<any>>();
    this.running = false;
  }

  get length() { return this.queue.length };

  push(task: () => Promise<any>): number {
    this.queue.push(task);
    if (!this.running) {
      this.running = true;
      this.processTasks();
    }

    return this.queue.length;
  }

  async processTasks() {
    const tasks = this.taskGenerator();
    for await (const task of tasks) { }
    this.running = false;
  }

  *taskGenerator() {
    while(this.length) {
      yield new Promise(async resolve => {
        const task = this.queue.shift();
        resolve(task());
      });
    }
  }
}