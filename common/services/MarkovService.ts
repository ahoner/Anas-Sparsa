import MarkovRepository from '@repos/MarkovRepository';

export default class MarkovService {
  markovRepo = new MarkovRepository();

  constructor(markovRepo: MarkovRepository) {
    this.markovRepo = markovRepo;
  }

  async addToModel(channelId: string, list: Array<string>): Promise<void> {
    console.log(`Adding new message to ${channelId}`);
    for (let i = 0; i < list.length; i++) {
      let key = `${list[i]} ${list[i + 1]}`;
      let value = list[i + 2];
      if (!value)
        break;

      await this.markovRepo.insertValue(channelId, key, value);
    }
  }

  async generateSentance(channelId: string, depth: number): Promise<string> {
    const keyCount = await this.markovRepo.keyCount(channelId);
    let keyIndex = Math.floor(Math.random() * keyCount);
    let key = await this.markovRepo.keyAtIndex(channelId, keyIndex);
    let markovModel = await this.markovRepo.getValuesAndRefs(channelId, key);
    let valueIndex = Math.floor(Math.random() * markovModel.values.length);
    let value = markovModel.values[valueIndex]
    const word = await this.generateSentanceHelper(channelId, key, value, depth);
    return `${key} ${value} ${word} `
  }

  async generateSentanceHelper(channelId: string, current: string, next: string, depth: number): Promise<string> {
    let key = `${current.split(' ')[1]} ${next}`;
    let markovModel = await this.markovRepo.getValuesAndRefs(channelId, key);
    if (!depth)
      return ''

    if (!markovModel.values.length)
      return await this.generateSentance(channelId, depth - 1);

    let valueIndex = Math.floor(Math.random() * markovModel.values.length);
    let value = markovModel.values[valueIndex];
    const word = await this.generateSentanceHelper(channelId, key, value, depth - 1);
    return `${value} ${word}`;
  }
}