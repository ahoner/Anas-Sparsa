import { openAi } from '@root/credentials.json';
import { Message } from 'discord.js';
import * as https from 'https';

interface MessageContext {
  author: string,
  contents: string
};

const url = 'api.openai.com';
const channelMessages: Map<string, MessageContext[]> = new Map();

export default class OpenAiService {

  addMessage(message: Message) {
    const authorId = message.author.id;
    const channelId = message.channel.id;
    const content = message.content;

    if (!channelMessages.get(channelId))
      channelMessages.set(channelId, []);

    const channelContext = channelMessages.get(channelId);
    if (channelContext.length >= 5)
      channelContext.pop();

    channelContext.unshift({
      author: authorId,
      contents: content
    });

    channelMessages.set(channelId, channelContext);
  }

  converse(message: Message): Promise<string> {
    let prompt = `The following is a conversation with humans and a duck that thinks it is a pirate. The duck likes to eat bread and drink rum and is quite hungry

Human: You are a good duck.
Duck: Yarrr, I be the greatest pirate captain the seven seas has ever seen. Every ship trembles when it sees my flag flown. I have vast holdings of gold and am quite fond of a good sea shanty
`
    const channelContext = channelMessages.get(message.channel.id);
    const authors = channelContext.reverse().map(context => ` ${context.author}:`);
    authors.unshift("\n");
    authors.push(" Duck:");
    channelContext.forEach(context => {
      prompt += `Human: ${context.contents}\n`;
    })
    prompt += `Duck:`

    console.log(prompt);
    console.log(authors);
    const data = {
      prompt: prompt,
      temperature: 0.9,
      max_tokens: 150,
      top_p: 1,
      frequency_penalty: 0,
      presence_penalty: 0.6,
      stop: ['\n', ' Human:', ' Duck:']
    };
    return this.post('/v1/engines/curie/completions', data);
  }

  joke(subject: string, place: string) {
    const prompt = `The following is a very funny joke

So ${subject} walks into ${place}`
    const data = {
      prompt: prompt,
      temperature: 0.9,
      max_tokens: 100,
      top_p: 1,
      frequency_penalty: 1,
      presence_penalty: 0.4,
      stop: ['The following is a very funny joke', `So ${subject} walks into ${place}`],
    };
    return this.post('/v1/engines/davinci/completions', data);
  }

  private post(endpoint: string, formData: any): Promise<string> {
    const options = {
      hostname: url,
      path: endpoint,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${openAi}`
      }
    };

    return new Promise<string>((resolve, reject) => {
      const req = https.request(options, res => {
        let data = '';
        res.on('data', chunk => {
          data += chunk;
        });

        res.on('end', () => {
          console.log(data);
          resolve(JSON.parse(data)?.choices[0]?.text);
        });
      });
      req.on('error', err => {
        reject(err);
      });

      req.write(JSON.stringify(formData));
      req.end();
    });
  }

}