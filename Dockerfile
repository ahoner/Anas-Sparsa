FROM node
WORKDIR /usr/anas-sparsa

# Install app dependencies
COPY package*.json ./

RUN npm install --only=production

# Bundle app source
COPY ./ ./
RUN cp ./node_modules/ffmpeg-static/ffmpeg ./

CMD npm start